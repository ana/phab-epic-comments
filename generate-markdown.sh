#!/bin/bash

# if you run this scripts with no arguments supplied it will just grab report
# for last week automatically, but if you call it like this:
#
# ./generate-markdown.sh --since 24/Sep/2018 --until 2/Oct/2018
#
# it will create the report for the dates provided

w=`expr $(date +%W) - 1`

txt=week$w.txt

[[ $# -eq 0 ]] && period="--week $w" || period="$@"

./phab-epic-comments.py --project GOO0010_core --nophab --title="Kernel" $period > $txt

./phab-epic-comments.py --project GOO0010_kernelci --nophab --title="KernelCI" $period >> $txt

sed -i -e "s/\[EPIC\] //g" $txt

echo -e "## Budget\n" >> $txt
echo -e "Week: XXX hours (USD)\n" >> $txt
echo -e "Year: total USD (XX%)\n" >> $txt
